
import { _decorator, Component, Node,Vec3,systemEvent,SystemEvent,EventMouse,Animation} from 'cc';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = PlayerController
 * DateTime = Sat Sep 18 2021 09:56:38 GMT+0800 (中国标准时间)
 * Author = pspring
 * FileBasename = PlayerController.ts
 * FileBasenameNoExtension = PlayerController
 * URL = db://assets/Scripts/PlayerController.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */

/** 鼠标点击value */
enum MouseClick{
    /** 左边 */
    Left,
    /** 中间滑轮 */
    Middle,
    /** 右边 */
    Right
}

@ccclass('PlayerController')
export class PlayerController extends Component {
    // [1]
    // dummy = '';
    /** 是否接收跳跃指令 */
    private _startJump: boolean = false;
    /** 跳跃步长 */
    private _jumpStep: number = 0;
    /**  当前跳跃时间 */
    private _curJumpTime: number = 0;
    /** 每次跳跃时长 */
    private _jumpTime: number = 0.1;
    /** 当前跳跃速度 */
    private _curJumpSpeed: number = 0;
    /**  当前角色位置 */
    private _curPos: Vec3 = new Vec3(0,0,0);
    /** 每次跳跃过程中，当前帧移动位置差 */
    private _deltaPos: Vec3 = new Vec3(0,0,0);
    /** 角色目标位置 */
    private _targetPos: Vec3 = new Vec3(0,0,0);
    /** 当前跳跃步数 */
    private _curMoveIndex = 0;


    // [2]
    // @property
    // serializableDummy = 0;
    @property({type: Animation})
    public BodyAnima: Animation | null = null;

    start () {
        // [3]
        // systemEvent.on(SystemEvent.EventType.MOUSE_UP,this.onMouseUp,this);
    }

    update (deltaTime: number) {
        // [4]
        if(this._startJump){
            this._curJumpTime += deltaTime;
            // 更新时间大于每次跳跃时间，更新坐标
            if(this._curJumpTime > this._jumpTime){
                // 更新坐标
                this.node.setPosition(this._targetPos);
                this._startJump = false;
                // 检测移动位置
                this.onOnceJumpEnd();
            }
            // 更新时间小于每次跳跃时间，更新当前坐标
            else{
                this.node.getPosition(this._curPos);
                // 计算跳跃距离
                this._deltaPos.x = this._curJumpSpeed * deltaTime;
                Vec3.add(this._curPos, this._curPos, this._deltaPos);
                this.node.setPosition(this._curPos);
            }
        }
    }

    setInputActive(active: boolean){
        if(active){
            systemEvent.on(SystemEvent.EventType.MOUSE_UP,this.onMouseUp,this);
        }else {
            systemEvent.off(SystemEvent.EventType.MOUSE_UP,this.onMouseUp,this);
        }
    }

    /** 对鼠标点击事件做处理 */
    onMouseUp(event: EventMouse){
        if(event.getButton() === MouseClick.Left){
            this.jumpByStep(1)
        }else if(event.getButton() === MouseClick.Right){
            this.jumpByStep(2)
        }
    }

    jumpByStep(step: number){
        if(this._startJump){
            return;
        }
        this._startJump = true;
        this._jumpStep = step;
        this._curJumpTime = 0;
        this._curJumpSpeed = this._jumpStep / this._jumpTime;
        this.node.getPosition(this._curPos);
        Vec3.add(this._targetPos, this._curPos, new Vec3(this._jumpStep, 0, 0));
        if(this.BodyAnima){
            if(step == 1){
                this.BodyAnima.play("oneStep");
            }else if(step == 2){
                this.BodyAnima.play("twoStep");
            }
        }
        this._curMoveIndex += step;
    }

    /** 每次跳跃结束检测  */
    onOnceJumpEnd(){
        this.node.emit("JumpEnd",this._curMoveIndex);
    }

    /** 重置人物位置 */
    reset() {
        this._curMoveIndex = 0;
        this._curPos = new Vec3(0,0,0);
        this._deltaPos = new Vec3(0,0,0);
        this._targetPos = new Vec3(0,0,0);
        this.node.setPosition(this._curPos);
    }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
