import {_decorator, Component, Node, Vec3} from 'cc';
const {ccclass, property} = _decorator;

/**
 * Predefined variables
 * Name = MoveSceneBackGround
 * DateTime = Tue Jan 25 2022 14:30:16 GMT+0800 (中国标准时间)
 * Author = pspring
 * FileBasename = MoveSceneBackground.ts
 * FileBasenameNoExtension = MoveSceneBackground
 * URL = db://assets/scripts/bg/MoveSceneBackground.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
@ccclass('MoveSceneBackground')
export class MoveSceneBackground extends Component {
    /** 移动速度 */
    private _bgMoveSpeed = 10;
    /** 移动上限 */
    private _bgMoveRange = 90;

    @property({type: Node})
    public background1: Node | null = null;

    @property({type: Node})
    public background2: Node | null = null;

    start() {
        this._init();
    }

    update(dt: number) {
        this._moveBackground(dt)
    }

    /** 初始化两个节点位置 */
    private _init() {
        this.background1.setPosition(0, 0, 0);
        this.background2.setPosition(0, 0, -this._bgMoveRange);
    }

    /**
     * 移动背景
     * @param dt
     * @private
     */
    private _moveBackground(dt: number) {
        // 设置位置
        this.background1.setPosition(0, 0, this.background1.getPosition().z + this._bgMoveSpeed * dt);
        this.background2.setPosition(0, 0, this.background2.getPosition().z + this._bgMoveSpeed * dt);
        // 判断是否查出界面
        if (this.background1.getPosition().z > this._bgMoveRange) {
            this.background1.setPosition(0, 0, this.background2.getPosition().z - this._bgMoveRange);
        } else if (this.background2.getPosition().z > this._bgMoveRange) {
            this.background2.setPosition(0, 0, this.background1.getPosition().z - this._bgMoveRange);
        }
    }


}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
