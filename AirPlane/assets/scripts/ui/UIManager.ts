
import {_decorator, CCInteger, Component, EventTouch, Node, SystemEvent, Touch} from 'cc';
import {GameManager} from "db://assets/scripts/framework/GameManager";
import {Base} from "db://assets/scripts/base/Base";
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = UIManager
 * DateTime = Tue Jan 25 2022 18:08:04 GMT+0800 (中国标准时间)
 * Author = pspring
 * FileBasename = UIManager.ts
 * FileBasenameNoExtension = UIManager
 * URL = db://assets/scripts/ui/UIManager.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
@ccclass('UIManager')
export class UIManager extends Component {

    @property({type: CCInteger})
    public planeSpeed : number | null = 3;

    @property({type: Node})
    public playerPlane : Node | null = null;

    @property(GameManager)
    public gameManager : GameManager | null = null;

    start () {
        this._listenerTouchEvent();
    }

    update (deltaTime: number) {
    }


    /**
     * 监听触摸事件
     * @private
     */
    private _listenerTouchEvent() {
        this.node.on(SystemEvent.EventType.TOUCH_START, this._touchStart, this);
        this.node.on(SystemEvent.EventType.TOUCH_MOVE, this._touchMove, this);
        this.node.on(SystemEvent.EventType.TOUCH_END, this._touchEnd, this);
    }

    private _touchStart(touch: Touch, event: EventTouch){
        this.gameManager.shooting(true);
    }

    private _touchMove(touch: Touch, event: EventTouch){
        let delta = touch.getDelta();
        let pos = this.playerPlane.getPosition();
        this.playerPlane.setPosition(pos.x + Base.PIXEL * this.planeSpeed * delta.x, pos.y, pos.z - Base.PIXEL * this.planeSpeed * delta.y);
    }

    private _touchEnd(touch: Touch, event: EventTouch){
        this.gameManager.shooting(false);
    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
