
import { _decorator, Component, Node} from 'cc';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = PlayerPlane
 * DateTime = Tue Jan 25 2022 17:41:06 GMT+0800 (中国标准时间)
 * Author = pspring
 * FileBasename = PlayerPlane.ts
 * FileBasenameNoExtension = PlayerPlane
 * URL = db://assets/scripts/plane/PlayerPlane.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
@ccclass('PlayerPlane')
export class PlayerPlane extends Component {


    start () {

    }

    update (deltaTime: number) {

    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
