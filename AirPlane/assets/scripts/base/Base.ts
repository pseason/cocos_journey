/**
 *
 */
export class Base {
    /** 像素单位 */
    static PIXEL: number = 0.01;
    /** 子弹屏幕移除屏幕的距离 */
    static OUT_SCREEN: number = -45;
    /** 子弹z轴离飞机的距离 */
    static BULLET_INTERVAL_PLANE: number = 7;
}