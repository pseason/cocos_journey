
import { _decorator, Component, Node, Prefab, CCInteger,instantiate} from 'cc';
import {Base} from "db://assets/scripts/base/Base";
import {Bullet} from "db://assets/scripts/bullet/Bullet";
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = GameManager
 * DateTime = Wed Jan 26 2022 13:34:35 GMT+0800 (中国标准时间)
 * Author = pspring
 * FileBasename = GameManager.ts
 * FileBasenameNoExtension = GameManager
 * URL = db://assets/scripts/framework/GameManager.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
 
@ccclass('GameManager')
export class GameManager extends Component {
    /** 玩家飞机 */
    @property(Node)
    public playerPlane: Node | null = null;
    /** 子弹管理节点 */
    @property(Node)
    public bulletManager: Node | null = null;
    /** bullet 1 - 5 为 子弹类型 */
    @property(Prefab)
    public bullet1: Prefab | null = null;
    @property(Prefab)
    public bullet2: Prefab | null = null;
    @property(Prefab)
    public bullet3: Prefab | null = null;
    @property(Prefab)
    public bullet4: Prefab | null = null;
    @property(Prefab)
    public bullet5: Prefab | null = null;
    /** 子弹射击时长 */
    @property
    public shootTime: number | null = 0.1;
    /** 子弹移动速度 */
    @property
    public bulletSpeed: number | null = 50;
    /** 当前射击时间 */
    private _curShortTime: number = 0;
    /** 当前是否射击 */
    private _isShooting: boolean = false;

    start () {
        this._init();
    }

    update (deltaTime: number) {
        this._curShortTime += deltaTime;
        if(this._isShooting && this._curShortTime > this.shootTime) {
            this.createPlayerBullet();
            this._curShortTime = 0;
        }
    }

    private _init(){
        this._curShortTime = this.shootTime
    }

    /**
     * 是否射击
     * @param boo
     */
    public shooting(boo: boolean){
        this._isShooting = boo;
    }

    /**
     * 1.创建玩家子弹，并放入父节点
     * 2.初始子弹位置、速度
     */
    createPlayerBullet(){
        // 1.创建玩家子弹，并放入父节点
        let bullet = instantiate(this.bullet1);
        bullet.setParent(this.bulletManager);
        // 2.初始子弹位置、速度
        let pos = this.playerPlane.getPosition();
        bullet.setPosition(pos.x,pos.y,pos.z - Base.BULLET_INTERVAL_PLANE)
        let bulletComponent = bullet.getComponent(Bullet);
        bulletComponent.bulletSpeed = this.bulletSpeed;
    }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
