
import { _decorator, Component, Node,CCInteger } from 'cc';
import {Base} from "db://assets/scripts/base/Base";
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = Bullet
 * DateTime = Tue Jan 25 2022 18:19:15 GMT+0800 (中国标准时间)
 * Author = pspring
 * FileBasename = Bullet.ts
 * FileBasenameNoExtension = Bullet
 * URL = db://assets/scripts/bullet/Bullet.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
@ccclass('Bullet')
export class Bullet extends Component {

    @property({ type: CCInteger })
    public bulletSpeed: number | null = 50;

    start () {
    }

    update (deltaTime: number) {
        let pos = this.node.getPosition();
        let moveLen = pos.z - this.bulletSpeed * deltaTime;
        this.node.setPosition(pos.x, pos.y, moveLen);
        if(moveLen < Base.OUT_SCREEN){
            this.node.destroy();
        }
    }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
